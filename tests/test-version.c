#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "eruntime/version.h"

static int t_eruntime_get_version (void);
static int t_eruntime_compare_versions (void);

typedef int (*test_case_func) (void);

typedef struct _test_case_struct
{
	const char	*name;
	const char	*desc;
	test_case_func	func;

} test_case_t;

/* {{{ static test_case_t version_tests[] = { ... } */
static test_case_t version_tests[] =
{
	{
		"eruntime_get_version",
		"function eruntime_get_version()",
		t_eruntime_get_version
	},
	{
		"eruntime_compare_versions",
		"function t_eruntime_compare_versions()",
		t_eruntime_compare_versions,
	},

	{NULL, NULL, NULL},
};
/* }}} */

/* {{{ int main() */
int
main (void)
{
	test_case_t	*tc = NULL;
	int		ret;

	setbuf(stdout, NULL);
	setbuf(stderr, NULL);

	for (tc = version_tests; tc->name; ++tc)
	{
		printf("Testing %s... ", tc->desc);
		ret = tc->func();

		if (ret == EXIT_SUCCESS)
			fputs("success\n", stdout);
		else
			fprintf(stderr, "\nTest '%s' failed\n", tc->name);
	}

	return EXIT_SUCCESS;
}
/* }}} */

/* {{{ static int t_eruntime_get_version() */
static int
t_eruntime_get_version (void)
{
	version_spec_t ev;

	if (eruntime_get_version(&ev) < 0)
	{
		fprintf(stderr, "eruntime_get_version() failed\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
/* }}} */

/* {{{ static int t_eruntime_compare_versions() */
static int
t_eruntime_compare_versions (void)
{
	version_spec_t	ev, ev_copy;
	int		sort_order;

	if (eruntime_get_version(&ev) < 0)
	{
		fprintf(stderr, "eruntime_get_version() failed\n");
		return EXIT_FAILURE;
	}

	memcpy(&ev_copy, &ev, sizeof(ev));
	sort_order = eruntime_compare_versions(&ev, &ev_copy);
	if (sort_order != 0)
	{
		fprintf(stderr, "\n\n"
			"sort order: %d\n"
			"expecting : 0\n",
			(int) sort_order);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80 :
 */
