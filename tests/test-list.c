#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "eruntime/list.h"

static int t_list_init (void);
static int t_list_create (void);
static int t_list_append (void);
static int t_list_prepend (void);
static int t_list_apply (void);

typedef int (*test_case_func) (void);

typedef struct _test_case_struct
{
	const char	*name;
	const char	*desc;
	test_case_func	func;

} test_case_t;

/* {{{ static test_case_t list_tests[] = { ... } */
static test_case_t list_tests[] =
{
	{
		"list_init",
		"function list_init()",
		t_list_init,
	},
	{
		"list_create",
		"function list_create()",
		t_list_create,
	},
	{
		"list_append",
		"function list_append()",
		t_list_append,
	},
	{
		"list_prepend",
		"function list_prepend()",
		t_list_prepend,
	},

	{NULL, NULL, NULL},
};
/* }}} */

/* {{{ int main() */
int
main (void)
{
	int i, ret;

	for (i = 0; list_tests[i].name; ++i)
	{
		printf("Testing %s...", list_tests[i].desc);

		ret = list_tests[i].func();
		printf(" %s\n", ret == EXIT_SUCCESS ? "success" : "failure");
		if (ret == EXIT_SUCCESS)
			continue;

		fprintf(stderr, "\n``` Test case '%s' failed.\n", list_tests[i].name);
	}

	return 0;
}
/* }}} */

/* {{{ #define LIST_INIT_IS_SANE() */
#define LIST_INIT_IS_SANE(list)				\
	do						\
	{						\
		list_t *__list = (list);		\
							\
		if (__list->length)			\
			return EXIT_FAILURE;		\
		if (__list->head || __list->tail)	\
			return EXIT_FAILURE;		\
	}						\
	while (0)
/* }}} */

/* {{{ static int t_list_init() */
static int
t_list_init (void)
{
	list_t list;

	if (list_init(&list) < 0)
		return EXIT_FAILURE;

	LIST_INIT_IS_SANE(&list);

	return EXIT_SUCCESS;
}
/* }}} */

/* {{{ static int t_list_create() */
static int
t_list_create (void)
{
	list_t *list = NULL;

	list = list_create();
	if (!list)
		return EXIT_FAILURE;

	LIST_INIT_IS_SANE(list);
	list_destroy(list);

	return EXIT_SUCCESS;
}
/* }}} */

/* {{{ static int t_list_append() */
static int
t_list_append (void)
{
	list_t		*list = NULL;
	list_node_t	*cur = NULL;
	char		*strings[] = {"afoo", "bfoo", "cfoo", NULL};
	int		i, c;

	list = list_create();
	if (!list)
		return EXIT_FAILURE;

	LIST_INIT_IS_SANE(list);

	for (i = 0; strings[i]; ++i)
	{
		if (list_append(list, strings[i]) < 0)
		{
			list_destroy(list);
			return EXIT_FAILURE;
		}
	}

	i = c = 0;
	LIST_FOREACH (list, cur)
	{
		if (cur->data == strings[i])
			++c;
		++i;
	}

	list_destroy(list);

	return c == 3 ? EXIT_SUCCESS : EXIT_FAILURE;
}
/* }}} */

/* {{{ static int t_list_prepend() */
static int
t_list_prepend (void)
{
	list_t		*list = NULL;
	list_node_t	*cur = NULL;
	char		*strings[] = {"cfoo", "bfoo", "afoo", NULL};
	int		i, c;

	list = list_create();
	if (!list)
		return EXIT_FAILURE;

	LIST_INIT_IS_SANE(list);

	for (i = 0; strings[i]; ++i)
	{
		if (list_prepend(list, strings[i]) < 0)
		{
			list_destroy(list);
			return EXIT_FAILURE;
		}
	}

	i = 2, c = 0;
	LIST_FOREACH (list, cur)
	{
		if (cur->data == strings[i])
			++c;
		--i;
	}

	list_destroy(list);

	return c == 3 ? EXIT_SUCCESS : EXIT_FAILURE;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
