topdir = ..
include $(topdir)/Makefile.top

all_test_NAMES = list version
all_test_SOURCES = $(addprefix test-,$(addsuffix .c,$(all_test_NAMES)))

all_TESTS = $(addsuffix .test, $(all_test_NAMES))
run_TESTS ?= $(all_TESTS)

.PHONY: all
all: $(all_TESTS)

.PHONY: deps
deps:
	$(MKDEPS) $(CC) '$(CFLAGS)' '$(all_test_SOURCES)'

%.test: test-%.c
	$(call wrap_LD,$@,$<,$(CFLAGS) -L$(topdir)/src -leruntime)

.PHONY: check
check: $(run_TESTS)
	@echo; list='$^';				\
	export LD_LIBRARY_PATH='$(topdir)/src';		\
	for test in $$list; do				\
		unit=$$(echo $$test |sed s/.test$$//);	\
		echo -e "\\n=== Testing unit: $$unit\\n"; \
		if $$PWD/$$test; then			\
			s=passed;			\
		else					\
			s=failed;			\
		fi;					\
		echo -ne "\\nUnit $$unit $$s\\n";	\
	done

.PHONY: clean
clean:
	-rm -f $(all_TESTS)

-include Makefile.deps

# vim: ts=8 sw=8 noet fdm=marker tw=80
