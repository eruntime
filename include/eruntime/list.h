#ifndef ERUNTIME_LIST_H
# define ERUNTIME_LIST_H

# include <inttypes.h>

# define LIST_FOREACH(list, cur) \
	for ((cur) = (list)->head; (cur); (cur) = (cur)->next)

typedef struct _list_node_struct list_node_t;

typedef struct _list_struct
{
	/* list head pointer. */
	list_node_t *head;

	/* list tail pointer. */
	list_node_t *tail;

	/* list length. */
	uint32_t length;

} list_t;

struct _list_node_struct
{
	/* pointer to previous node in the chain. */
	list_node_t *prev;

	/* pointer to next node in the chain. */
	list_node_t *next;

	/* pointer to node data. */
	void *data;
};

typedef void (*list_apply_func_t) (void *, void *);

int8_t	list_init (list_t *list);
list_t	*list_create (void);
int8_t	list_append (list_t *list, void *data);
int8_t	list_prepend (list_t *list, void *data);
int32_t list_apply (list_t *list, list_apply_func_t func, void *arg);
void	list_clear (list_t *list);
void	list_destroy (list_t *list);

#endif /* ! ERUNTIME_LIST_H */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
