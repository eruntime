#ifndef ERUNTIME_SMALLOC_H
# define ERUNTIME_SMALLOC_H

# include <stddef.h>

# ifdef __ERUNTIME__
typedef struct _oom_data_struct
{
	enum
	{
		OOM_SMALLOC

	} caller;

	union
	{
		size_t size;

	} a;

} oom_data_t;
# endif /* ! __ERUNTIME__ */

void *smalloc (size_t);
void sfree (void *);

#endif /* ! ERUNTIME_SMALLOC_H */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
