#include <inttypes.h>

typedef struct _version_spec_struct
{
	uint16_t major;
	uint16_t minor;
	uint32_t patch;
	uint32_t build;
	const char *extra_str;

} version_spec_t;

int8_t eruntime_get_version (version_spec_t *);
int8_t eruntime_compare_versions (version_spec_t *, version_spec_t *);

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
