#include <assert.h>
#include <inttypes.h>
#include <string.h>

#include "eruntime/version.h"

/* {{{ static version_spec_t eruntime_version = { ... } */
version_spec_t eruntime_version =
{
	.major = 0,
	.minor = 1,
	.patch = 0,
	.build = 0,
	.extra_str = "",
};
/* }}} */

/* {{{ int8_t eruntime_get_version() */
int8_t
eruntime_get_version (version_spec_t *vspec)
{
	assert(vspec);
	if (!vspec)
		return -1;

	memcpy(vspec, &eruntime_version, sizeof(*vspec));
	return 0;
}
/* }}} */

/* {{{ int8_t eruntime_compare_versions() */
int8_t
eruntime_compare_versions (version_spec_t *vs_a, version_spec_t *vs_b)
{
	assert(vs_a && vs_b);
	if (!vs_a || !vs_b)
		return -1;

	if (vs_a->major > vs_b->major)
		return 1;
	else if (vs_a->major < vs_b->major)
		return -1;

	if (vs_a->minor > vs_b->minor)
		return 1;
	else if (vs_a->minor < vs_b->minor)
		return -1;

	if (vs_a->patch > vs_b->patch)
		return 1;
	else if (vs_a->patch < vs_b->patch)
		return -1;

	if (vs_a->build > vs_b->build)
		return 1;
	else if (vs_a->build < vs_b->build)
		return -1;

	/*
	 * TODO: Add support for comparing against ->extra_str.
	 */

	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
