#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include "eruntime/list.h"
#include "eruntime/smalloc.h"

#ifndef errno
extern int errno;
#endif

/*
 * list_init()    - statically initialise a list object.
 * list_create()  - create a new linked list object.
 * list_append()  - append an item to the head of the list.
 * list_prepend() - prepend an item to the tail of the list.
 * list_apply()   - apply a function to all items in a list.
 * list_clear()   - clear (free) all nodes in a list.
 * list_destroy() - free memory associated with list object and its nodes.
 */

/* {{{ int8_t list_init() */
int8_t
list_init (list_t *list)
{
	assert(list);
	if (!list)
	{
		errno = EINVAL;
		return -1;
	}

	list->head = NULL;
	list->tail = NULL;
	list->length = 0;

	return 0;
}
/* }}} */

/* {{{ list_t *list_create() */
list_t *
list_create (void)
{
	list_t *list = NULL;

	list = smalloc(sizeof(*list));
	if (list_init(list) < 0)
	{
		sfree(list);
		return NULL;
	}

	return list;
}
/* }}} */

/* {{{ int8_t list_append(list, data) */
int8_t
list_append (list_t *list, void *data)
{
	list_node_t *new = NULL;

	assert(list && data);
	if (!list || !data)
	{
		errno = EINVAL;
		return -1;
	}

	new = smalloc(sizeof(*new));
	new->prev = NULL;
	new->next = NULL;
	new->data = data;

	if (!list->head)
	{
		list->head = new;
		list->tail = list->head;
	}
	else
	{
		new->prev = list->tail;
		list->tail->next = new;
		list->tail = new;
	}

	++list->length;
	return 0;
}
/* }}} */

/* {{{ int8_t list_prepend(list, data) */
int8_t
list_prepend (list_t *list, void *data)
{
	list_node_t *new = NULL;

	assert(list && data);
	if (!list || !data)
	{
		errno = EINVAL;
		return -1;
	}

	new = smalloc(sizeof(*new));
	new->prev = NULL;
	new->next = NULL;
	new->data = data;

	if (!list->head)
	{
		list->head = new;
		list->tail = list->head;
	}
	else
	{
		new->next = list->head;
		list->head->prev = new;
		list->head = new;
	}

	++list->length;
	return 0;
}
/* }}} */

/* {{{ int32_t list_apply(list, func, arg) */
int32_t
list_apply (list_t *list, list_apply_func_t func, void *arg)
{
	list_node_t	*cur = NULL;
	int32_t		i;

	assert(list && func);
	if (!list || !func)
	{
		errno = EINVAL;
		return -1;
	}

	/* Empty list? */
	if (!list->head)
		return 0;

	i = 0;
	LIST_FOREACH (list, cur)
	{
		func(cur->data, arg);
		++i;
	}

	return i;
}
/* }}} */

/* {{{ void list_clear() */
void
list_clear (list_t *list)
{
	list_node_t *cur = NULL;
	list_node_t *next = NULL;

	assert(list);
	if (!list || !list->head)
		return;

	cur = list->head;
	while (cur)
	{
		next = cur->next;
		sfree(cur);
		cur = next;
		--list->length;
	}
}
/* }}} */

/* {{{ void list_destroy() */
void
list_destroy (list_t *list)
{
	assert(list);
	if (!list)
		return;

	if (list->head)
		list_clear(list);
	sfree(list);
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
