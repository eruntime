#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "eruntime/smalloc.h"

#ifndef errno
extern into errno;
#endif

#define __noreturn __attribute__((__noreturn__))
static void __noreturn oom (oom_data_t *);

/* {{{ void *smalloc() */
void *
smalloc (size_t size)
{
	void		*p = NULL;
	oom_data_t	oom_data;

	assert(size);
	if (!size)
	{
		size = 1;
		fprintf(stderr, "WARNING: %s() called with size argument of '0', "
			"falling back to '1'", __FUNCTION__);
	}

	p = malloc(size);
	if (p)
		return p;

	oom_data.caller = OOM_SMALLOC;
	oom_data.a.size = size;
	oom(&oom_data);
}
/* }}} */

/* {{{ void sfree() */
void
sfree (void *ptr)
{
	if (!ptr)
		return;
	free(ptr);
}
/* }}} */

/* {{{ static void oom() */
static void
oom (oom_data_t *oom_data)
{
	char buf[8] = {0}; /* strlen("realloc") + 1 */

	assert(oom_data);

	if (oom_data->caller == OOM_SMALLOC)
	{
		strcpy(buf, "malloc");
		buf[6] = '\0';
	}
	else
	{
		fprintf(stderr, "ERROR: %s() called with invalid ->caller '%d'",
			__FUNCTION__, oom_data->caller);
		abort();
	}

	fprintf(stderr, "ERROR: Out Of Memory Condition; %s() aborting",
		__FUNCTION__);

	if (*buf)
		fprintf(stderr, "ERROR: %s\n", buf);
	if (errno)
		fprintf(stderr, "ERROR: %s (errno %d)", strerror(errno), errno);

	fflush(stdout);
	fflush(stderr);

	abort();
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
