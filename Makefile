# Top-level Makefile

include Makefile.top

SUBDIRS = src
CLEANDIRS = $(SUBDIRS) tests

.PHONY: all all-recursive
all: all-recursive

all-recursive: $(SUBDIRS)
	@list='$^'; for dir in $$list; do	\
		echo -e "\\n... Building $$dir";\
		$(SUBMAKE) -C $$dir || exit 1;	\
	done

.PHONY: deps deps-recursive
deps: deps-recursive

deps-recursive: $(SUBDIRS)
	@list='$^'; for dir in $$list; do					\
		echo -e "\\n... Generating dependency information for $$dir";	\
		$(SUBMAKE) -C $$dir deps || exit 1;				\
	done

.PHONY: install install-local install-top install-bin install-headers \
	install-recursive

install: install-top install-local install-recursive
install-local: install-bin install-headers

install-top:
	@echo -e "\\n... Installing from top-level"

install-bin: eruntime-config
	@dir='$(DESTDIR)$(bindir)';				\
	$(INSTALL_DIR) $(DESTDIR)$(bindir) || exit 1;		\
	list='$^'; for bin in $$list; do			\
		echo "Installing $(DESTDIR)$(bindir)/$$bin...";	\
		$(INSTALL_BIN) $$bin $$dir || exit 1;		\
	done

install-headers:
	@$(INSTALL_DIR) $(DESTDIR)$(includedir)/eruntime || exit 1;		\
	list="$$(echo include/eruntime/*.h)";					\
	[ "$$list" != 'include/eruntime/*.h' ] && {				\
		for hdr in $$list; do						\
			echo "Installing $(DESTDIR)$(includedir)/$$hdr...";	\
			$(INSTALL_DATA) $$hdr $(DESTDIR)$(includedir)/eruntime	\
				|| exit 1;					\
		done;								\
	}

install-recursive: $(SUBDIRS)
	@list='$^'; for dir in $$list; do		\
		echo -e "\\n... Installing from $$dir";	\
		$(SUBMAKE) -C $$dir install || exit 1;	\
	done

.PHONY: check
check: all tests
	@echo -e "\\n... Building tests";	\
	$(SUBMAKE) -C tests all || exit 1;	\
	echo -e "\\n... Running tests";		\
	$(SUBMAKE) -C tests check

.PHONY: clean clean-recursive
clean: clean-recursive

clean-recursive: $(CLEANDIRS)
	@echo; list='$^';				\
	for dir in $$list; do				\
		echo -e "\\n... Cleaning $$dir";	\
		$(SUBMAKE) -C $$dir clean || exit 1;	\
	done

# vim: ts=8 sw=8 noet fdm=marker tw=80
