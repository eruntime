#!/bin/bash
#
# {{{
#
# Copyright (C) 2006 Elfyn McBratney.
# All rights reserved.
#
# This file is part of eruntime.
#
# License:
#
#	eruntime is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License, version 2,
#	as published by the Free Software Foundation.
#
#	eruntime is distributed in the hope that it will be useful, but
#	WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with eruntime; if not, write to:
#
#		Free Software Foundation, Inc.
#		59 Temple Place
#		Suite 330
#		Boston, MA 02111-1307
#		USA
#
# Authors:
#
#	Elfyn McBratney <elfyn.mcbratney@gmail.com>
#
# }}}

prog="$(basename $0)"

force=no
configure=no

# {{{ usage()
usage() {
	echo "Usage: $prog [options...]"
	echo "  -f, --force  Force run of \`autoconf' and \`autoheader'"
	echo "  -c, --conf   Run \`configure' after bootstrapping"
	echo "  -h, --help   Display usage information"
	exit 0
}
# }}}

# {{{ die()
die() {
	echo "${prog}: error: $*" >&2
	exit 1
}
# }}}

while [[ $# > 0 ]]; do
	arg="$1"
	shift

	case "${arg}" in
		-f|--force)
		force=yes
		;;

		-c|--conf)
		configure=yes
		break 2
		;;

		-h|--help)
		usage
		;;

		*)
		die "invalid argument \`${arg}'"
		;;
	esac
done

[[ -e configure.ac ]] || \
	die "this script must be run from top-level source directory"

if [[ ${force} == yes || ! -e configure || configure.ac -nt configure ]]; then
	echo "bootstrapping..."
	for tool in auto{conf,header}; do
		${tool} || die "${tool} failed"
	done
fi

if [[ ${configure} == yes ]]; then
	echo "configuring..."
	./configure "$@" || die "configure failed"
fi

exit 0

#
# vim: ts=8 sw=8 noet fdm=marker tw=80
#
